package nl.utwente.di.tempTranslator;

public class Translator {

    public double getBookPrice(String isbn){
        double result = Double.parseDouble(isbn);
        return (result * (9.0/5.0) + 32);
    }

}
